
package System;

public class Carro implements Veiculo{

    private int Combustivel;
    private int Rend;
    private int MaxCarg;
    private int VelMed;
    double PerdaRend;

    public Carro() {
        Combustivel = 3;
        Rend = 14;
        MaxCarg = 360;
        VelMed = 100;
        PerdaRend = 0.025;
    }
    public int getCombustivel(){
        return Combustivel;
    }

    public int getRend(){
        return Rend;
    }

    public int getCarga(){
        return MaxCarg;
    }

    public int getVel(){
        return VelMed;
    }

    public double getPerdaRend(){
        return PerdaRend;
    }
}

