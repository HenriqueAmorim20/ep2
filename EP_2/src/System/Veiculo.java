
package System;

public interface Veiculo {
    
    int getCombustivel();

    int getRend();

    int getCarga();

    int getVel();

    double getPerdaRend();
 
    
}
