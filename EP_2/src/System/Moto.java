
package System;

public class Moto implements Veiculo{

    private int Combustivel;
    private int Rend;
    private int MaxCarg;
    private int VelMed;
    double PerdaRend;

    public Moto() {
        Combustivel = 3;
        Rend = 50;
        MaxCarg = 50;
        VelMed = 110;
        PerdaRend = 0.3;
    }
    public int getCombustivel(){
        return Combustivel;
    }

    public int getRend(){
        return Rend;
    }

    public int getCarga(){
        return MaxCarg;
    }

    public int getVel(){
        return VelMed;
    }

    public double getPerdaRend(){
        return PerdaRend;
    }
}

