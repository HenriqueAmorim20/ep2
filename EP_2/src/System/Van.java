
package System;

public class Van implements Veiculo{

    private int Combustivel;
    private int Rend;
    private int MaxCarg;
    private int VelMed;
    double PerdaRend;

    public Van() {
        Combustivel = 2;
        Rend = 10;
        MaxCarg = 3500;
        VelMed = 80;
        PerdaRend = 0.001;
    }
    public int getCombustivel(){
        return Combustivel;
    }

    public int getRend(){
        return Rend;
    }

    public int getCarga(){
        return MaxCarg;
    }

    public int getVel(){
        return VelMed;
    }

    public double getPerdaRend(){
        return PerdaRend;
    }
}

