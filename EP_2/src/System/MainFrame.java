
package System;

import java.io.File;
import java.io.FileNotFoundException;
import static java.time.zone.ZoneRulesProvider.refresh;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JPopupMenu;

public class MainFrame extends javax.swing.JFrame {

   
    public MainFrame() {
        initComponents();
    }

    private int carreta;
    private int van;
    private int carro;
    private int moto;

    public int getCarreta() {
        return carreta;
    }

    public void setCarreta(int carreta) {
        this.carreta = carreta;
    }

    public int getVan() {
        return van;
    }

    public void setVan(int van) {
        this.van = van;
    }

    public int getCarro() {
        return carro;
    }

    public void setCarro(int carro) {
        this.carro = carro;
    }

    public int getMoto() {
        return moto;
    }

    public void setMoto(int moto) {
        this.moto = moto;
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {
        
        try {
              
            //new Thread(t1).start();
            jScrollPane2 = new javax.swing.JScrollPane(); // </editor-fold>
            ListaVeiculos = new javax.swing.JList<>();
            Bfrete = new javax.swing.JButton();
            Badd = new javax.swing.JButton();
            Bremover = new javax.swing.JButton();
            Bliberar = new javax.swing.JButton();
            jScrollPane1 = new javax.swing.JScrollPane();
            Tabela = new javax.swing.JTable();
            Blucro = new javax.swing.JButton();
            jButton1 = new javax.swing.JButton();
            setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
            setTitle("HTransportadora");
            
            ListaVeiculos.setModel(new javax.swing.AbstractListModel<String>() {
                
                
                File Carreta = new File("Carreta.txt");
                File Van = new File("Van.txt");
                File Carro = new File("Carro.txt");
                File Moto = new File("Moto.txt");
                File Lucro = new File("Lucro.txt");
                
                
                Scanner cainput = new Scanner(Carreta);
                Scanner vninput = new Scanner(Van);
                Scanner coinput = new Scanner(Carro);
                Scanner moinput = new Scanner(Moto);
                Scanner luinput = new Scanner(Lucro);
                
                
                int carreta = cainput.nextInt();
                int van = vninput.nextInt();
                int carro = coinput.nextInt();
                int moto = moinput.nextInt();
                int lucro = luinput.nextInt();

                
                
                String[] strings = { "Carreta x"+carreta, "Van x"+van, "Carro x"+carro, "Moto x"+moto, "Lucro Atual: "+lucro,"%\n"};
                public int getSize() { return strings.length; }
                public String getElementAt(int i) { return strings[i]; }
            });
            jScrollPane2.setViewportView(ListaVeiculos);
            Bfrete.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
            Bfrete.setText("Novo Frete");
            Bfrete.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    BfreteActionPerformed(evt);
                }
            });
            Badd.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
            Badd.setText("Adicionar Veículo");
            Badd.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    BaddActionPerformed(evt);
                }
            });
            Bremover.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
            Bremover.setText("Remover Veículo");
            Bremover.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    BremoverActionPerformed(evt);
                }
            });
            Bliberar.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
            Bliberar.setText("Liberar Veículo");
            Tabela.setModel(new javax.swing.table.DefaultTableModel(
                    new Object [][] {
                        
                    },
                    new String [] {
                        
                    }
            ));
            jScrollPane1.setViewportView(Tabela);
            Blucro.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
            Blucro.setText("Lucro");
             Blucro.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    BlucroActionPerformed(evt);
                }
            });
            jButton1.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
            jButton1.setText("Atualizar");
            jButton1.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jButton1ActionPerformed(evt);
                }
            });
            javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
            getContentPane().setLayout(layout);
            layout.setHorizontalGroup(
                    layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                    .addContainerGap()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addGroup(layout.createSequentialGroup()
                                                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 392, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 17, Short.MAX_VALUE)
                                                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                            .addComponent(Bfrete, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                            .addComponent(Bliberar, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                    .addGap(88, 88, 88)
                                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                            .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                            .addComponent(Blucro, javax.swing.GroupLayout.DEFAULT_SIZE, 90, Short.MAX_VALUE))
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                            .addComponent(Badd, javax.swing.GroupLayout.Alignment.TRAILING)
                                                            .addComponent(Bremover, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                    .addContainerGap())
            );
            layout.setVerticalGroup(
                    layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                    .addContainerGap()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 134, Short.MAX_VALUE))
                                    .addGap(22, 22, 22)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(Badd)
                                            .addComponent(Bfrete)
                                            .addComponent(Blucro))
                                    .addGap(26, 26, 26)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(Bremover)
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                    .addComponent(Bliberar)
                                                    .addComponent(jButton1)))
                                    .addContainerGap(27, Short.MAX_VALUE))
            );
            pack();
            setLocationRelativeTo(null);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        updateMainList();
        new Thread(t1).start();
    }

    private void BaddActionPerformed(java.awt.event.ActionEvent evt) {                                     
        java.awt.EventQueue.invokeLater(new Runnable() {
                public void run() {
                 new Add().setVisible(true);
                }
            });
    }                                    

    private void BremoverActionPerformed(java.awt.event.ActionEvent evt) {                                         
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Remover().setVisible(true);
            }
        });
    }                                        

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {                                         
        updateMainList();
    }  
     private Runnable t1 = new Runnable() {
        public void run() {
            try{
                for(;;){
                updateMainList();
                System.out.println("up");
                Thread.sleep(1000);
                }
            } catch (Exception e){
                System.out.println("e");
            }
 
        }
    };
      public void updateMainList(){
         
        try {
            File Carreta = new File("Carreta.txt");
            File Van = new File("Van.txt");
            File Carro = new File("Carro.txt");
            File Moto = new File("Moto.txt");
            File Lucro = new File("Lucro.txt");
            
            
            Scanner cainput = new Scanner(Carreta);
            Scanner vninput = new Scanner(Van);
            Scanner coinput = new Scanner(Carro);
            Scanner moinput = new Scanner(Moto);
            Scanner luinput = new Scanner(Lucro);
            
            
            int carreta = cainput.nextInt();
            int van = vninput.nextInt();
            int carro = coinput.nextInt();
            int moto = moinput.nextInt();
            int lucro = luinput.nextInt();
            
            DefaultListModel DefModel = new DefaultListModel();
            DefModel.addElement("Carreta  " + "x"+carreta);
            DefModel.addElement("Van  " + "x"+van);
            DefModel.addElement("Carro  " + "x"+carro); 
            DefModel.addElement("Moto  " + "x"+moto);
            DefModel.addElement("Lucro Atual: " + lucro+"%");

            ListaVeiculos.setModel(DefModel);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        }
    private void BlucroActionPerformed(java.awt.event.ActionEvent evt) {                                                                                
               java.awt.EventQueue.invokeLater(new Runnable() {
                public void run() {
                 new Lucro().setVisible(true);
                }
            });
    } 
    private void BfreteActionPerformed(java.awt.event.ActionEvent evt) {                                                                                
               java.awt.EventQueue.invokeLater(new Runnable() {
                public void run() {
                 new Frete().setVisible(true);
                }
            });
    }


    // Variables declaration - do not modify                     
    private javax.swing.JButton Badd;
    private javax.swing.JButton Bfrete;
    private javax.swing.JButton Bliberar;
    private javax.swing.JButton Blucro;
    private javax.swing.JButton Bremover;
    public javax.swing.JList<String> ListaVeiculos;
    private javax.swing.JTable Tabela;
    private javax.swing.JButton jButton1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    // End of variables declaration                   
}
