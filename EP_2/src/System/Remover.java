
package System;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Remover extends javax.swing.JFrame {

    private int carreta;
    private int van;
    private int carro;
    private int moto;
    
    public Remover() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList<>();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jList1.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Carreta", "Van", "Carro", "Moto"};
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane1.setViewportView(jList1);

        jButton1.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jButton1.setText("Remover");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(31, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addGap(54, 54, 54))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
       String s = (String) jList1.getSelectedValue();
        
        
        
        if (s.equals("Carreta")){
            File teste = new File("Carreta.txt");
            try {
                Scanner input = new Scanner(teste);
                               
                carreta = input.nextInt();
                if(carreta==0){
                    carreta=0;
                }else{
                carreta = carreta -1;
                }
                
                
            } catch (FileNotFoundException ex) {
                System.out.println("e");
            }
            try {
                PrintWriter output =  new PrintWriter(teste);
                
                output.println(carreta);
                output.close();
                
            } catch (FileNotFoundException ex) {
                System.out.println("e");
            }
        }
        else if (s.equals("Van")){
            File teste = new File("Van.txt");
            try {
                Scanner input = new Scanner(teste);
                               
                van = input.nextInt();
                if(van==0){
                    van=0;
                }else{
                van = van -1;
                }
                
            } catch (FileNotFoundException ex) {
                System.out.println("e");
            }
            try {
                PrintWriter output =  new PrintWriter(teste);
                
                output.println(van);
                output.close();
                
            } catch (FileNotFoundException ex) {
                System.out.println("e");
            }
        }
        else if (s.equals("Carro")){
            File teste = new File("Carro.txt");
            try {
                Scanner input = new Scanner(teste);
                               
                carro = input.nextInt();
                if(carro==0){
                    carro=0;
                }
                else{
                carro = carro -1;
                }
                
                
            } catch (FileNotFoundException ex) {
                System.out.println("e");
            }
            try {
                PrintWriter output =  new PrintWriter(teste);
                
                output.println(carro);
                output.close();
                
            } catch (FileNotFoundException ex) {
                System.out.println("e");
            }
        }
        else if (s.equals("Moto")){
            File teste = new File("Moto.txt");
            try {
                Scanner input = new Scanner(teste);
                               
                moto = input.nextInt();
                if(moto==0) {
                    moto=0;
                } else {
                    moto = moto -1;
                }
                
                
            } catch (FileNotFoundException ex) {
                System.out.println("e");
            }
            try {
                PrintWriter output =  new PrintWriter(teste);
                
                output.println(moto);
                output.close();
                
            } catch (FileNotFoundException ex) {
                System.out.println("e");
            }
        }
        this.dispose();
    
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JList<String> jList1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
