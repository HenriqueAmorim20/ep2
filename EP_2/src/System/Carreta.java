
package System;

public class Carreta implements Veiculo{

    private int Combustivel;
    private int Rend;
    private int MaxCarg;
    private int VelMed;
    double PerdaRend;

    public Carreta() {
        Combustivel = 2;
        Rend = 8;
        MaxCarg = 30000;
        VelMed = 60;
        PerdaRend = 0.0002;
    }
    public int getCombustivel(){
        return Combustivel;
    }

    public int getRend(){
        return Rend;
    }

    public int getCarga(){
        return MaxCarg;
    }

    public int getVel(){
        return VelMed;
    }

    public double getPerdaRend(){
        return PerdaRend;
    }
}

